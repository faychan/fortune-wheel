 
// Import Application class that is the main part of our PIXI project
import { Application } from '@pixi/app'

// In order that PIXI could render things we need to register appropriate plugins
import { Renderer } from '@pixi/core' // Renderer is the class that is going to register plugins

import { BatchRenderer } from '@pixi/core' // BatchRenderer is the "plugin" for drawing sprites
Renderer.registerPlugin('batch', BatchRenderer)

import { TickerPlugin } from '@pixi/ticker' // TickerPlugin is the plugin for running an update loop (it's for the application class)
Application.registerPlugin(TickerPlugin)

// And just for convenience let's register Loader plugin in order to use it right from Application instance like app.loader.add(..) etc.
import { AppLoaderPlugin } from '@pixi/loaders'
Application.registerPlugin(AppLoaderPlugin)

// Sprite is our image on the stage
import { Sprite } from '@pixi/sprite'

var options = ["5% off", "So Close!!", "10% off", "No prize for you", "20% off", "No luck today","Free Shipping", "You can't win 'em all", "15% off", "You've got bad luck, pal"];

var startAngle = 0;
var arc = Math.PI / (options.length / 2);
var spinTimeout = null;

var spinArcStart = 10;
var spinTime = 0;
var spinTimeTotal = 0;
var spinAngleStart;

var ctx;

var el = document.getElementById("spin");
if(el){
	el.addEventListener("click", spin);
}

function drawRouletteWheel() {
  var canvas = document.getElementById("canvas");
  if (canvas){
		if (canvas.getContext) {
			var outsideRadius = 250;
			var textRadius = 180;
			var insideRadius = 100;

			ctx = canvas.getContext("2d");
			ctx.clearRect(0,0,500,500);

			ctx.font = 'bold 12px Helvetica, Arial';

			for(var i = 0; i < options.length; i++) {
				var angle = startAngle + i * arc;
				if(i%2 == 0){
					ctx.fillStyle = "#f8b195";
				} else{
					ctx.fillStyle = "#f67280";
				}

				ctx.beginPath();
				ctx.arc(300, 300, outsideRadius, angle, angle + arc, false);
				ctx.arc(300, 300, insideRadius, angle + arc, angle, true);
				ctx.stroke();
				ctx.fill();

				ctx.save();
				ctx.shadowOffsetX = -1;
				ctx.shadowOffsetY = -1;
				ctx.shadowBlur    = 0;
				ctx.shadowColor   = "rgb(220,220,220)";
				ctx.fillStyle = "black";
				ctx.translate(300 + Math.cos(angle + arc / 2) * textRadius, 
											300 + Math.sin(angle + arc / 2) * textRadius);
				ctx.rotate(20 * Math.PI / 180);
				var text = options[i];
				ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
				ctx.restore();
			} 

			//Arrow
			ctx.fillStyle = "black";
			ctx.beginPath();
			ctx.moveTo(300 - 4, 300 - (outsideRadius + 5));
			ctx.lineTo(300 + 4, 300 - (outsideRadius + 5));
			ctx.lineTo(300 + 4, 300 - (outsideRadius - 5));
			ctx.lineTo(300 + 9, 300 - (outsideRadius - 5));
			ctx.lineTo(300 + 0, 300 - (outsideRadius - 13));
			ctx.lineTo(300 - 9, 300 - (outsideRadius - 5));
			ctx.lineTo(300 - 4, 300 - (outsideRadius - 5));
			ctx.lineTo(300 - 4, 300 - (outsideRadius + 5));
			ctx.fill();
		}
	}
}

function spin() {
  spinAngleStart = Math.random() * 10 + 10;
  spinTime = 0;
  spinTimeTotal = Math.random() * 3 + 4 * 1000;
  rotateWheel();
}

function rotateWheel() {
  spinTime += 30;
  if(spinTime >= spinTimeTotal) {
    stopRotateWheel();
    return;
  }
  var spinAngle = spinAngleStart - easeOut(spinTime, 0, spinAngleStart, spinTimeTotal);
  startAngle += (spinAngle * Math.PI / 180);
  drawRouletteWheel();
  spinTimeout = setTimeout(()=>{ rotateWheel() }, 30);
}

function stopRotateWheel() {
  clearTimeout(spinTimeout);
  var degrees = startAngle * 180 / Math.PI + 90;
  var arcd = arc * 180 / Math.PI;
  var index = Math.floor((360 - degrees % 360) / arcd);
  ctx.save();
  ctx.font = 'bold 20px Helvetica, Arial';
  var text = options[index]
  ctx.fillText(text, 300 - ctx.measureText(text).width / 2, 300 + 10);
  ctx.restore();
}

function easeOut(t, b, c, d) {
  var ts = (t/=d)*t;
  var tc = ts*t;
  return b+c*(tc + -3*ts + 3*t);
}

drawRouletteWheel();